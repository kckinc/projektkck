import re
import spellChecker

step = 32
koniunktory = [' a nastepnie ', ' pozniej ', ' nastepnie ', ' a pozniej ', ' potem ', ' i ', ' a potem ', ' oraz ']


def textProc(text, indiana, canvas, indianaimg, bgimg, root, pts, consolearea):

    for word in str(text).split():
        if word not in open('dict.txt','r').read():
            text = text.replace(word, spellChecker.correct(word))

    for substring in koniunktory:
        if substring in text:
            for subtext in text.split(substring):
                textProc(str(subtext).replace(substring, ""), indiana, canvas, indianaimg, bgimg, root, pts, consolearea)

    # if any(substring in text for substring in koniunktory):
    #     str(text).split(substring , 1)

    if not any(s in text for s in koniunktory):
        if re.match(r'[praw[o|ej]]?|[lew[o|ej|a]]?|[gor[e|a|y]]?|[dolu?]?', text) is not None:
            return moveCommand(text, indiana, canvas, indianaimg, bgimg, root, pts, consolearea)
    else:
        return text


def moveCommand(text, indiana, canvas, indianaimg, bgimg, root, pts, consolearea):
    text = numberNormalizer(text)
    multiplier = [1]
    #x = indiana.iCoordinates[0]
    #y = indiana.iCoordinates[1]
    m = re.match(r'.*?(\d).*?', text)
    if m is not None:
        multiplier[0] = int(m.group(1))

    prawo = re.match(r'.*praw[o|ej].*', text)
    if prawo is not None:
        for i in range (0, multiplier[0]):
            indiana.go_east(canvas, indiana, indianaimg, bgimg, root, pts, consolearea)
    lewo = re.match(r'.*lew[o|ej].*', text)
    if lewo is not None:
        for i in range (0, multiplier[0]):
            indiana.go_west(canvas, indiana, indianaimg, consolearea)
    gora = re.match(r'.*gor[e|a|y].*', text)
    if gora is not None:
        for i in range (0, multiplier[0]):
            indiana.go_north(canvas, indiana, indianaimg, consolearea)
    dol = re.match(r'.*dolu?.*', text)
    if dol is not None:
        for i in range (0, multiplier[0]):
            indiana.go_south(canvas, indiana, indianaimg, consolearea)
    #indiana.iCoordinates[0] = indiana.position[0]
    #indiana.iCoordinates[1] = indiana.position[1]
    return text

def numberNormalizer(text):
    text = str(text).replace('jeden ', ' 1 ')
    text = str(text).replace('dwa ', ' 2 ')
    text = str(text).replace('trzy ', ' 3 ')
    text = str(text).replace('cztery ', ' 4 ')
    text = str(text).replace('piec ', ' 5 ')
    text = str(text).replace('szesc ', ' 6 ')
    text = str(text).replace('siedem ', ' 7 ')
    return text
