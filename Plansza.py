from __future__ import print_function
import numpy as np
from numpy.random import rand, randint, seed
from random import randint
import pygame
class Board:
    
                #################################################
                #                                               #
                #   Podstawowa wersja algorytmu generujacego    #
                #   plansze. Dla ulatwienia dalszych testow     #
                #   ograniczylem nieco losowosc planszy. Mimo   #
                #   to rozwiazanie generuje grywalne, rozniace  #
                #   sie od siebie poziomy.                      #
                #                                               #
                #################################################

    def __init__(self):
        self.board = np.zeros((10, 10))
        self.board = self.board.astype(int)
        for i in range(0, 10):
            self.board[i][0] = 1
            self.board[0][i] = 1
            self.board[9][i] = 1
            self.board[i][9] = 1

        seed()
        temp_Start = randint(1, 8)
        self.board[0][temp_Start] = 7
        self.board[9][9 - temp_Start] = 8

                #################################################
                #                                               #
                #   Te paskudnie dlugie warunki sprawdzaja      #
                #   czy w danym miejscu moze byc przeszkoda     #
                #                                               #
                #################################################

        x = temp_Start
        y = 1
        for j in range(1, 8):
            for i in range(1, 8):
                if self.board[j][i] == 0 and randint(1, 100) > 43+i+j+i+j and (self.board[i-1][j] == 0 or self.board[i][j-1] == 0 or self.board[i+1][j] == 0 or self.board[i][j+1] == 0) and (self.board[i-1][j] != 7 and self.board[i][j-1] != 7 and self.board[i+1][j] != 7 and self.board[i][j+1] != 7) and (self.board[i-1][j] != 8 and self.board[i][j-1] != 8 and self.board[i+1][j] != 8 and  self.board[i][j+1] != 8):
                    self.board[j][i] = 1

        self.board[1][temp_Start] = 0
        for j in range(1, 8):
            for i in range(1, 8):
                if self.board[j][i] == 1 and self.board[i-1][j] == 1 and self.board[i][j-1] == 1 and self.board[i+1][j] == 1 and self.board[i][j+1] == 1:
                    self.board[j][i] = 0

        for j in range(1, 8):
            for i in range(1, 8):
                if self.board[j][i] == 0 and not(self.board[i-1][j] == 0 and ((self.board[i][j-1] == 0 or self.board[i+1][j] == 0 or self.board[i][j+1] == 0) or (self.board[i][j-1] == 0 or self.board[i+1][j] == 0 or self.board[i][j+1] == 0) or (self.board[i+1][j] == 0 or self.board[i][j+1] == 0))):
                    self.board[j][i+1] = 0
    visited=[]
    chokepoint_H=[]
    chokepoint_V=[]
    global images
    #images = [gui.Image('assets/textures/grass.jpg'),gui.Image('assets/textures/tree.png')] PGUI

    def find_start(self):
        #krotka=[0,0]
        krotka=[np.where(self.board==7)[0][0],np.where(self.board==7)[1][0]]
        #krotka[0]=np.where(self.board==7)[0]
        #krotka[1]=np.where(self.board==7)[1]
        return krotka#[np.where(matrix==1)[0][0]+1,np.where(matrix==1)[1][0]]

                #############################################
                #                                           #
                #   Metody poruszania sie po macierzy.      #
                #   Znaczaca zmniejszaja liczbe bledow      #
                #   generowanych przez bledny zapis         #
                #                                           #
                #############################################


    def go_south(self, pos):
        return [pos[0]+1, pos[1]]

    def go_north(self, pos):
        return [pos[0]-1, pos[1]]

    def go_east(self, pos):
        return [pos[0], pos[1]+1]

    def go_west(self, pos):
        return [pos[0], pos[1]-1]
       
    def get(self, pos):
        return self.board[pos[0], pos[1]]

                #################################################
                #                                               #
                #   Oznaczenie komorki jako odwiedzonej jest    #
                #   kluczowe dla funkcji sprawdzajacej macierz  #
                #   a metoda uncharted sprawdza, czy dana       #
                #   komorka nadaje sie do odwiedzenia i         #
                #   jednoczesnie nie zostala odwiedzona         #
                #   wczesniej.                                  #
                #                                               #
                #################################################


    def mark_as_visited(self, pos):
        self.visited.append(pos)

    def uncharted(self, pos):
        return (self.get(pos)==0 or self.get(pos)==7 or self.get(pos)==8) and pos not in self.visited
        
    def printer(self):
        print(self.board)
        
    def draw_board(self):
        dim = (500, 500)
        pole = pygame.Surface(dim)
        tree = pygame.image.load('assets/textures/tree1.png')
        grass = pygame.image.load('assets/textures/grass2.png')
        entrance = pygame.image.load('assets/textures/entrance.png')
        pole.blit(pygame.image.load('assets/textures/grass' + str(randint(1,3)) + '.png'), (0,0))
        for i in range (0, 500, 50):
            for j in range (0, 500, 50):
                # pole.blit(pygame.image.load('assets/textures/grass' + str(randint(1,4)) + '.png'), (i,j))
                if self.board[i/50][j/50] == 1:    
                    pole.blit(pygame.image.load('assets/textures/tree' + str(randint(1,6)) + '.png'), (i,j))
                else:
                    if i == 450 or i == 0:
                        pole.blit(entrance, (i,j))
                """
                if ((i == 0) or (i == 288) and (self.board[i/32][j/32] == 0):
                    pole.blit(indiana entrance, (i,j))
                """
        pygame.image.save(pole, 'pole.png')

                #################################################
                #                                               #
                #   Podstawowa metoda calej klasy. Sluzy        #
                #   weryfikacji poprawnosci macierzy.           #
                #   Rekurencyjnie wywoluje sie dla kazdej jej   #
                #   komorki poczynajac od wejscia i sprawdza,   #
                #   czy macierz da sie przejsc przy uzyciu      #
                #   predefiniowanych ruchow.                    #
                #                                               #
                #################################################

    def trailblazer(self, pos):
        if self.uncharted(pos):
            self.mark_as_visited(pos)
            # print(pos) # DEBUG
            if self.get(pos) == 8:
                return 1
            return self.trailblazer(self.go_north(pos)) or self.trailblazer(self.go_south(pos)) or self.trailblazer(self.go_east(pos)) or self.trailblazer(self.go_west(pos))
            
                #################################################
                #                                               #
                #   Metoda szukaja waskich gardel na planszy    #
                #   na podstawie sasiedztwa punktow. Waskie     #
                #   gardlo musi miec trzy wolne pola w linii    #
                #   i byc ograniczone po bokach. Metoda przyda  #
                #   sie do umieszczania potworow i skarbow      #
                #                                               #
                #################################################

    def find_chokepoints(self):
        for i in range (2, 7):
            for j in range (2, 7):
                if self.board[i,j]==0 and self.board[i+1,j] + self.board[i-1,j] == 2 and self.board[i, j+1] + self.board[i, j+1] == 0:
                    self.chokepoint_H.append([i,j])
                elif self.board[i,j]==0 and self.board[i, j+1] + self.board[i, j+1] == 2 and self.board[i+1,j] + self.board[i-1,j] == 0:
                    self.chokepoint_V.append([i,j])


        
global board            
board = Board()
while(board.trailblazer(board.find_start()) != 1):
    print(board.trailblazer(board.find_start()))
    board = Board()
