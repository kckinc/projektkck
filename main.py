import tkinter
from tkinter import *
import aiml
import textProcessor
import Indiana
from Plansza import board
from PIL import Image
from pointcounter import points

# ===== INICJACJA ==============================

root = Tk()
root.geometry("800x600+50+50")
root.resizable(0,0)
canvas = tkinter.Canvas(root, width=800, height=600)
canvas.pack()
remaining = 180
# points = 0

# ===== MENU ===================================

# utworzenie paska menu
menu_bar = Menu(root)
# utworzenie podmenu
file_menu = Menu(menu_bar, tearoff=0)
# dodanie elementow podmenu
file_menu.add_command(label="Wyjście", command=root.destroy)
# nazwa podmenu
menu_bar.add_cascade(label="Menu", menu=file_menu)

root.config(menu=menu_bar)

# ===== MAPA ===================================

"""
base_img = PhotoImage(file="assets/textures/new_grass.png")
base_img_label = Label(root, image=base_img)
base_img_label.pack()
"""


# indianaimg = tkinter.PhotoImage(file="assets/textures/indiana.png")
# treeimg = tkinter.PhotoImage(file="assets/textures/tree.png")
# canvas.create_image(12, 24, image=indianaimg)
# canvas.create_image(160, 160, image=treeimg)

board.draw_board()

im_temp = Image.open("pole.png")
im_temp = im_temp.resize((500, 500), Image.ANTIALIAS)
im_temp.save("pole_resized.png", "png")
backgroundimg = tkinter.PhotoImage(file="pole_resized.png")
bgimg = canvas.create_image(250, 250, image=backgroundimg, tag="plansza")

# ===== KONSOLA ================================

consolearea = Text(root, wrap=tkinter.WORD)
consolearea.insert("insert", "Witaj poszukiwaczu skarbów!")
# consolearea.pack()
consolearea.place(x = 510, y = 2, width=280, height=560)

# ===== INDIANA ================================

indiana = Indiana.Indiana()
indianaimg = tkinter.PhotoImage(file="assets/textures/indiana.png")
indiana.indianaDraw(canvas, indianaimg)

"""
# ===== EKWIPUNEK ==============================

equipmentimg = tkinter.PhotoImage(file="assets/textures/equipment.png")
canvas.create_image(250, 550, image=equipmentimg)
"""

# ===== ODLICZANIE =============================
cnt = Label(root, text="Pozostały czas: ", font=("Helvetica", 16))
cnt.place(x = 10, y = 510)

pts = Label(root, text="Zdobyte punkty: 0", font=("Helvetica", 16))
pts.place(x = 10, y = 550)

def countdown():
    global remaining
    if remaining <= 0:
        print("czas minal!!!")
        gameoverimg = tkinter.PhotoImage(file="assets/textures/game_over.png")
        canvas.create_image(250, 250, image=gameoverimg)
        gameoverimg.pack()
    else:
        remaining = remaining - 1
        # print(remaining)
        cnt['text'] = "Pozostały czas: " + str(remaining) + " sekund."
        # pts['text'] = "Zdobyte punkty: " + str(points)
        # print("punkty: " + str(points))
        root.after(1000, countdown)

# ===== WPROWADZANIE KOMEND ====================
entrytextvar = StringVar()


def entrytoconsole(event):
    # print(entrytextvar.get())
    global entrytextvar
    # consolearea.insert("insert", "\n" + entrytextvar.get())
    execcommand()
    consolearea.see(tkinter.END)
    entry.delete(0, 'end')


def execcommand():
    global entrytextvar
    consolearea.insert("insert", "\n" + entrytextvar.get())
    textProcessor.textProc(entrytextvar.get(), indiana, canvas, indianaimg, bgimg, root, pts, consolearea)

entry = Entry(root, textvariable=entrytextvar)
# entry.pack()
entry.place(x = 510, y = 570, width=280, height=25)
entry.bind('<Return>', entrytoconsole)
entry.bind('<Control-x>', lambda e: 'break') #disable cut
entry.bind('<Control-c>', lambda e: 'break') #disable copy
entry.bind('<Control-v>', lambda e: 'break') #disable paste
entry.bind('<Button-3>', lambda e: 'break')  #disable right-click

# ===== GLOWNA PETLA ===========================

countdown()
root.mainloop()
