import Plansza
from Plansza import board
import tkinter
from tkinter import *
from PIL import Image
from numpy.random import seed
from pointcounter import points

class Indiana:
    # indianaImg = gui.Image('assets/textures/Indiana.png')
    global TODO_MULTIPLIER
    TODO_MULTIPLIER = 50
    global position
    global board
    position = board.find_start()
    global iCoordinates
    iCoordinates = [position[0] * TODO_MULTIPLIER, position[1] * TODO_MULTIPLIER]


    def __init__(self):
        global board
        global TODO_MULTIPLIER
        # indianaImg = gui.Image('assets/textures/Indiana.png')
        position = board.find_start()
        print(position)
        iCoordinates = [position[0] * TODO_MULTIPLIER, position[1] * TODO_MULTIPLIER]


    def indianaDraw(self, canvas, indianaimg):
        self.canvas = canvas
        self.indianaimg = indianaimg
        global iCoordinates
        print("drawing indiana")
        # iCoordinates = [position[0] * TODO_MULTIPLIER, position[1] * TODO_MULTIPLIER]
        # draws Indiana on coordinates x,y in specified element
        canvas.delete(canvas.find_withtag('indiana'))
        canvas.create_image(int(iCoordinates[0]) + 25, int(iCoordinates[1]) + 25, image=indianaimg, tag="indiana")

    def drawIndianaOnStart(self, canvas, indianaimg, position):
        self.canvas = canvas
        self.indianaimg = indianaimg
        global iCoordinates
        print("drawing indiana")
        canvas.delete(canvas.find_withtag('indiana'))
        canvas.create_image(int((position[0] * 50)) + 25, int((position[1] * 50)) + 25, image=indianaimg, tag="indiana")


   ###############################################################     
   #                                                             #
   #    Funkcja pozwalajaca oceniac odleglosc                    #
   #                                                             #
   ###############################################################     
   # def look_south(self)                                       
   #     global sight
   #     sight = position
   #     counter = 0    
   #     global board
   #     while (board.get([sight[0], sight[1]+1]) == 0:
   #         counter = counter+1
   #         sight[1]=sight[1]+1
   ################################################################

    def look_south(self):                                   
        #global sight
        sight = self.position
        counter = 0    
        global board
        while (board.get([sight[0], sight[1]+1])== 0):
            counter = counter+1
            sight = [sight[0], sight[1]+1]
            look_south(self, sight)
        return ["W odleglosci " + str(counter) + " na poludnie widze przeszkode", counter]

    def look_north(self):                                       
        #global sight
        sight = self.position
        counter = 0    
        global board
        while (board.get([sight[0], sight[1]-1]) == 0):
            counter = counter+1
            sight = [sight[0], sight[1]-1]
            look_north(self, sight)
        return ["W odleglosci " + str(counter) + " na polnoc widze przeszkode", counter]

    def look_east(self):                                     
        #global sight
        sight = self.position
        counter = 0    
        global board
        while (board.get([sight[0]+1, sight[1]]) == 0):
            counter = counter+1
            sight = [sight[0]+1, sight[1]]
            look_east(self, sight)
        return ["W odleglosci " + str(counter) + " na wschod widze przeszkode", counter]

    def look_west(self):                                       
        #global sight
        sight = self.position
        counter = 0
        global board
        while (board.get([sight[0]-1, sight[1]]) == 0):
            counter = counter+1
            sight = [sight[0]-1, sight[1]]
            look_west(self, sight)
        return ["W odleglosci " + str(counter) + " na zachod widze przeszkode", counter]

    def find_closest(self):
        nwse = [look_north()[1], look_west()[1], look_south()[1], look_east()[1]]
        if min(nwse) == nwse[0]:
            return "Idz na polnoc " + str(counter)
        if min(nwse) == nwse[1]:
            return "Idz na zachod " + str(counter)
        if min(nwse) == nwse[2]:
            return "Idz na poludnie " + str(counter)
        if min(nwse) == nwse[3]:
            return "Idz na wschod " + str(counter)

    def go_south(self, canvas, indiana, indianaimg, consolearea):
        global position
        global TODO_MULTIPLIER
        global iCoordinates
        global board
        if board.get([position[0], position[1]+1]) == 0:
            position = [position[0], position[1]+1]
            iCoordinates = [position[0]*TODO_MULTIPLIER, position[1]*TODO_MULTIPLIER]
            print(position)
            print(iCoordinates)
            self.indianaDraw(canvas, indianaimg)
        else:
            consolearea.insert("insert", "\nNie moge tego zrobic")

    def go_north(self, canvas, indiana, indianaimg, consolearea):
        global position
        global TODO_MULTIPLIER
        global iCoordinates
        global board
        if board.get([position[0], position[1]-1]) == 0:
            position = [position[0], position[1]-1]
            iCoordinates = [position[0]*TODO_MULTIPLIER, position[1]*TODO_MULTIPLIER]
            print(position)
            print(iCoordinates)
            self.indianaDraw(canvas, indianaimg)
        else:
            consolearea.insert("insert", "\nNie moge tego zrobic")

    def go_east(self, canvas, indiana, indianaimg, bgimg, root, pts, consolearea):
        global position
        global TODO_MULTIPLIER
        global iCoordinates
        global board
        if board.get([position[0]+1, position[1]]) == 0:
            position = [position[0]+1, position[1]]
            iCoordinates = [position[0]*TODO_MULTIPLIER, position[1]*TODO_MULTIPLIER]
            print(position)
            print(iCoordinates)
            self.indianaDraw(canvas, indianaimg)
        elif board.get([position[0]+1, position[1]]) == 8:
            global points
            print(points)
            points = points + 1
            pts['text'] = "Zdobyte punkty: " + str(points)
            board = Plansza.Board()
            canvas.delete("all")
            board.draw_board()
            position = board.find_start()
            print("pozycja: " + str(position))
            im_temp = Image.open("pole.png")
            im_temp = im_temp.resize((500, 500), Image.ANTIALIAS)
            im_temp.save("pole_resized.png", "png")
            backgroundimg = tkinter.PhotoImage(file="pole_resized.png")
            root.backgroundimg = backgroundimg
            bgimg = canvas.create_image(250, 250, image=backgroundimg, tag="plansza")
            # canvas.tag_raise("plansza")
            # canvas.tag_raise("indiana")
            # self.drawIndianaOnStart(canvas, indianaimg, position)
            self.go_east(canvas, indiana, indianaimg, bgimg, root, pts, consolearea)

        else:
            consolearea.insert("insert", "\nNie moge tego zrobic")

    def go_west(self, canvas, indiana, indianaimg, consolearea):
        global position
        global TODO_MULTIPLIER
        global iCoordinates
        global board
        if board.get([position[0]-1, position[1]]) == 0:
            position = [position[0]-1, position[1]]
            iCoordinates = [position[0]*TODO_MULTIPLIER, position[1]*TODO_MULTIPLIER]
            print(position)
            print(iCoordinates)
            self.indianaDraw(canvas, indianaimg)
        else:
            consolearea.insert("insert", "\nNie moge tego zrobic")
